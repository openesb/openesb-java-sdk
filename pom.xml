<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>net.open-esb</groupId>
        <artifactId>parent</artifactId>
        <version>1.0.2</version>
    </parent>
    
    <groupId>net.open-esb.admin</groupId>
    <artifactId>openesb-java-sdk</artifactId>
    <version>1.0.4-SNAPSHOT</version>

    <name>OpenESB SDK for Java</name>
    <description>
        OpenESB SDK for Java provides Java APIs to manage OpenESB instances.
    </description>

    <scm>
        <url>git@bitbucket.org:openesb/openesb-java-sdk.git</url>
        <connection>scm:git:git@bitbucket.org:openesb/openesb-java-sdk.git</connection>
        <developerConnection>scm:git:git@bitbucket.org:openesb/openesb-java-sdk.git</developerConnection>
    </scm>

    <profiles>
        <profile>
            <id>releases</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-release-plugin</artifactId>
                        <version>2.5.1</version>
                        <configuration>
                            <useReleaseProfile>false</useReleaseProfile>
                            <releaseProfiles>release</releaseProfiles>
                            <goals>deploy</goals>
                            <autoVersionSubmodules>true</autoVersionSubmodules>
                            <arguments>-DskipTests</arguments>
                        </configuration>
                    </plugin>

                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>

                    <plugin>
                        <groupId>org.sonatype.plugins</groupId>
                        <artifactId>nexus-staging-maven-plugin</artifactId>
                        <version>1.6.8</version>
                        <executions>
                            <execution>
                                <id>default-deploy</id>
                                <phase>deploy</phase>
                                <goals>
                                    <goal>deploy</goal>
                                </goals>
                            </execution>
                        </executions>
                        <configuration>
                            <serverId>nexus-community</serverId>
                            <nexusUrl>https://nexus.open-esb.net/repository/maven-releases</nexusUrl>
                            <skipStaging>true</skipStaging>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <configuration>
                    <failOnError>false</failOnError>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-site-plugin</artifactId>
                <version>3.3</version>
                <configuration>
                    <reportPlugins>
                        <plugin>
                            <groupId>org.apache.maven.plugins</groupId>
                            <artifactId>maven-javadoc-plugin</artifactId>
                            <configuration>
                                <additionalparam>-Xdoclint:none</additionalparam>
                            </configuration>
                        </plugin>
                    </reportPlugins>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.7.0</version>
                <configuration>
                    <source>1.7</source>
                    <target>1.7</target>
                    <!--
                    <debug>true</debug>
                    <debuglevel>none</debuglevel>
                    -->
                </configuration>
            </plugin>
        </plugins>
    </build>

    <dependencies>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.11</version>
            <scope>test</scope>
        </dependency>
        
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpclient</artifactId>
            <version>${httpcomponents.version}</version>
        </dependency>
        
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpmime</artifactId>
            <version>${httpcomponents.version}</version>
        </dependency>
        
        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-core-asl</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        
        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-mapper-asl</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>${commons-lang3}</version>
        </dependency>
        
        <dependency>
            <groupId>net.open-esb</groupId>
            <artifactId>openesb-model-api</artifactId>
            <version>${openesb-model-api.version}</version>
        </dependency>
    </dependencies>

    <distributionManagement>
        <snapshotRepository>
            <id>nexus-community</id>
            <url>https://nexus.open-esb.net/repository/maven-snapshots</url>
        </snapshotRepository>

        <repository>
            <id>nexus-community</id>
            <url>https://nexus.open-esb.net/repository/maven-releases</url>
        </repository>
    </distributionManagement>

    <properties>
        <jackson.version>1.9.13</jackson.version>
        <httpcomponents.version>4.3.1</httpcomponents.version>
        <openesb-model-api.version>1.0.0</openesb-model-api.version>
        <commons-lang3>3.2.1</commons-lang3>
    </properties>
</project>
