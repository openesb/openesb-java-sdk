package net.openesb.sdk.http;

import net.openesb.sdk.Request;
import net.openesb.sdk.ClientConfiguration;
import net.openesb.sdk.OpenESBClientException;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;

/**
 * Responsible for creating Apache HttpClient 4 request objects.
 * 
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class HttpRequestFactory {

    HttpRequestBase createHttpRequest(Request<?> request, ClientConfiguration clientConfiguration, HttpEntity previousEntity) {
        String uri = request.getResourcePath();
        
        HttpRequestBase httpRequest;
        if (request.getHttpMethod() == HttpMethodName.POST) {
            HttpPost postMethod = new HttpPost(uri);
            
            httpRequest = postMethod;
        } else if (request.getHttpMethod() == HttpMethodName.GET) {
            httpRequest = new HttpGet(uri);
        } else if (request.getHttpMethod() == HttpMethodName.DELETE) {
            httpRequest = new HttpDelete(uri);
        } else if (request.getHttpMethod() == HttpMethodName.HEAD) {
            httpRequest = new HttpHead(uri);
        } else {
            throw new OpenESBClientException("Unknown HTTP method name: " + request.getHttpMethod());
        }
        
        return httpRequest;
    }
}
