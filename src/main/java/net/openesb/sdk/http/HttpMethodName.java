package net.openesb.sdk.http;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public enum HttpMethodName {
    GET, POST, PUT, DELETE, HEAD;
}
