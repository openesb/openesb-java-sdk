package net.openesb.sdk.http;


/**
 * Responsible for handling an HTTP response and returning an object of type T.
 * For example, a typical response handler might accept a response, and
 * translate it into a concrete typed object.
 * 
 * @param <T>
 *            The output of this response handler.
 */
public interface HttpResponseHandler<T> {

    /**
     * Accepts an HTTP response object, and returns an object of type T.
     * Individual implementations may choose to handle the response however they
     * need to, and return any type that they need to.
     * 
     * @param response
     *            The HTTP response to handle, as received from an AWS service.
     * 
     * @return An object of type T, as defined by individual implementations.
     * 
     * @throws Exception
     *             If any problems are encountered handling the response.
     */
    public T handle(HttpResponse response) throws Exception;
}

