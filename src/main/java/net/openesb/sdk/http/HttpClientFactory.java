package net.openesb.sdk.http;

import net.openesb.sdk.ClientConfiguration;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class HttpClientFactory {

    public HttpClient createHttpClient(ClientConfiguration config) {
        HttpClient client = HttpClients.createDefault();
        
        // TODO: custom configuration with proxy, ...
        
        return client;
    }
}
