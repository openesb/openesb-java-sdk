package net.openesb.sdk;

import java.io.InputStream;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import net.openesb.sdk.http.HttpMethodName;

/**
 * Default implementation of the {@linkplain net.openesb.sdk.Request} interface.
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class DefaultRequest<T> implements Request<T> {

    /**
     * The resource path being requested
     */
    private String resourcePath;
    
    /**
     * Map of the parameters being sent as part of this request
     */
    private Map<String, String> parameters = new HashMap<String, String>();
    
    /**
     * Map of the headers included in this request
     */
    private Map<String, String> headers = new HashMap<String, String>();
    
    /**
     * The service endpoint to which this request should be sent
     */
    private URI endpoint;

    /**
     * The HTTP method to use when sending this request.
     */
    private HttpMethodName httpMethod = HttpMethodName.POST;
    
    /**
     * An optional stream from which to read the request payload.
     */
    private InputStream content;

    /**
     * @see net.openesb.sdk.Request#addHeader(java.lang.String, java.lang.String)
     */
    public void addHeader(String name, String value) {
        headers.put(name, value);
    }

    /**
     * @see net.openesb.sdk.Request#getHeaders()
     */
    public Map<String, String> getHeaders() {
        return headers;
    }

    /**
     * @see net.openesb.sdk.Request#setResourcePath(java.lang.String)
     */
    public void setResourcePath(String resourcePath) {
        this.resourcePath = resourcePath;
    }

    /**
     * @see net.openesb.sdk.Request#getResourcePath()
     */
    public String getResourcePath() {
        return resourcePath;
    }

    /**
     * @see net.openesb.sdk.Request#addParameter(java.lang.String,
     * java.lang.String)
     */
    public void addParameter(String name, String value) {
        parameters.put(name, value);
    }

    /**
     * @see net.openesb.sdk.Request#getParameters()
     */
    public Map<String, String> getParameters() {
        return parameters;
    }

    /**
     * @see net.openesb.sdk.Request#withParameter(java.lang.String,
     * java.lang.String)
     */
    public Request<T> withParameter(String name, String value) {
        addParameter(name, value);
        return this;
    }

    /**
     * @see net.openesb.sdk.Request#getHttpMethod()
     */
    public HttpMethodName getHttpMethod() {
        return httpMethod;
    }

    /**
     * @see
     * net.openesb.sdk.Request#setHttpMethod(net.openesb.sdk.http.HttpMethodName)
     */
    public void setHttpMethod(HttpMethodName httpMethod) {
        this.httpMethod = httpMethod;
    }

    /**
     * @see net.openesb.sdk.Request#setEndpoint(java.net.URI)
     */
    public void setEndpoint(URI endpoint) {
        this.endpoint = endpoint;
    }

    /**
     * @see net.openesb.sdk.Request#getEndpoint()
     */
    public URI getEndpoint() {
        return endpoint;
    }

    /**
     * @see net.openesb.sdk.Request#getContent()
     */
    public InputStream getContent() {
        return content;
    }

    /**
     * @see net.openesb.sdk.Request#setContent(java.io.InputStream)
     */
    public void setContent(InputStream content) {
        this.content = content;
    }

    /**
     * @see net.openesb.sdk.Request#setHeaders(java.util.Map)
     */
    public void setHeaders(Map<String, String> headers) {
        this.headers.clear();
        this.headers.putAll(headers);
    }

    /**
     * @see net.openesb.sdk.Request#setParameters(java.util.Map)
     */
    public void setParameters(Map<String, String> parameters) {
        this.parameters.clear();
        this.parameters.putAll(parameters);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append(getHttpMethod().toString()).append(" ");
        builder.append(getEndpoint().toString()).append(" ");

        builder.append("/").append(getResourcePath() != null ? getResourcePath() : "").append(" ");

        if (!getParameters().isEmpty()) {
            builder.append("Parameters: (");
            for (String key : getParameters().keySet()) {
                String value = getParameters().get(key);
                builder.append(key).append(": ").append(value).append(", ");
            }
            builder.append(") ");
        }

        if (!getHeaders().isEmpty()) {
            builder.append("Headers: (");
            for (String key : getHeaders().keySet()) {
                String value = getHeaders().get(key);
                builder.append(key).append(": ").append(value).append(", ");
            }
            builder.append(") ");
        }

        return builder.toString();
    }
}
