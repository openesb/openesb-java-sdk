package net.openesb.sdk;

import java.net.URI;

/**
 *
 * @author david
 */
public class ClientConfiguration {
    
    public final static String DEFAULT_URI = "http://localhost:4848/openesb/api";
    
    private String serverApiUrl = "http://localhost:4848/openesb/api";
    
    private String proxyHost;
    private int proxyPort;
    private String proxyUser;
    private String proxyPassword;

    public ClientConfiguration(String serverApiUrl) {
        this.serverApiUrl = serverApiUrl;
    }

    /**
     * Starts with a default plain connection configuration that talks to our public production API endpoint.
     */
    public ClientConfiguration() {
        this(DEFAULT_URI);
    }
    
    public String getServerApiUrl() {
        return serverApiUrl;
    }
    
    public String getPath() {
        return URI.create(getServerApiUrl()).getPath();
    }

    public void setServerApiUrl(String serverApiUrl) {
        this.serverApiUrl = serverApiUrl;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public void setProxyHost(String proxyHost) {
        this.proxyHost = proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(int proxyPort) {
        this.proxyPort = proxyPort;
    }

    public String getProxyUser() {
        return proxyUser;
    }

    public void setProxyUser(String proxyUser) {
        this.proxyUser = proxyUser;
    }

    public String getProxyPassword() {
        return proxyPassword;
    }

    public void setProxyPassword(String proxyPassword) {
        this.proxyPassword = proxyPassword;
    }
}
