package net.openesb.sdk;

/**
 * Base exception class for any errors that occur while attempting to use an 
 * OpenESB Client to make service calls to the Rest API.
 * 
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class OpenESBClientException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new OpenESBClientException with the specified message, and root
     * cause.
     * 
     * @param message
     *            An error message describing why this exception was thrown.
     * @param t
     *            The underlying cause of this exception.
     */
    public OpenESBClientException(String message, Throwable t) {
        super(message, t);
    }

    /**
     * Creates a new OpenESBClientException with the specified message.
     * 
     * @param message
     *            An error message describing why this exception was thrown.
     */
    public OpenESBClientException(String message) {
        super(message);
    }

}
