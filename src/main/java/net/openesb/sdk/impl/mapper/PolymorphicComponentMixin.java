package net.openesb.sdk.impl.mapper;


import net.openesb.model.api.ServiceEngine;
import net.openesb.model.api.BindingComponent;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@JsonTypeInfo(  
    use = JsonTypeInfo.Id.NAME,  
    include = JsonTypeInfo.As.PROPERTY,  
    property = "type")  
@JsonSubTypes({  
    @JsonSubTypes.Type(value = ServiceEngine.class, name = "SERVICE_ENGINE"),  
    @JsonSubTypes.Type(value = BindingComponent.class, name = "BINDING_COMPONENT") })
public class PolymorphicComponentMixin {
    
}
