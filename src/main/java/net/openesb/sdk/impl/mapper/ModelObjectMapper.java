package net.openesb.sdk.impl.mapper;

import net.openesb.model.api.JBIComponent;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ModelObjectMapper extends ObjectMapper {

    public ModelObjectMapper() {
        super();
        this.getDeserializationConfig().addMixInAnnotations(
                JBIComponent.class, PolymorphicComponentMixin.class);
    }
}
