package net.openesb.sdk.model;

import java.util.Map;
import net.openesb.sdk.http.HttpMethodName;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StartComponentRequest extends AbstractRequest {
    
    private final String componentName;
    
    public StartComponentRequest(final String componentName) {
        this.componentName = componentName;
    }

    public String getComponentName() {
        return componentName;
    }

    @Override
    public HttpMethodName method() {
        return HttpMethodName.POST;
    }

    @Override
    public String uri() {
        return "/components/" + getComponentName();
    }

    @Override
    public Map<String, String> parameters() {
        Map<String, String> parameters = super.parameters();
        parameters.put("action", "start");
        return parameters;
    }
}
