package net.openesb.sdk.model;

import java.util.Map;
import net.openesb.model.api.Logger;
import net.openesb.sdk.http.HttpMethodName;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class SetComponentLoggerRequest extends AbstractRequest {
    
    private final String componentName;
    private Logger logger;
    
    public SetComponentLoggerRequest(String componentName, Logger logger) {
        this.componentName = componentName;
        this.logger = logger;
    }

    public String getComponentName() {
        return componentName;
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Override
    public HttpMethodName method() {
        return HttpMethodName.PUT;
    }

    @Override
    public String uri() {
        return "/components/" + getComponentName() + "/loggers";
    }

    @Override
    public Map<String, String> parameters() {
        Map<String, String> parameters =  super.parameters();
     
        parameters.put("logger", getLogger().getName());
        parameters.put("level", getLogger().getLevel());
                
        return parameters;
    }
}
