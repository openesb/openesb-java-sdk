package net.openesb.sdk.model;

import java.util.Map;
import net.openesb.sdk.http.HttpMethodName;
import org.apache.http.HttpHeaders;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class LoginRequest extends AbstractRequest {
    private final String username;
    private final String password;
    
    public LoginRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public HttpMethodName method() {
        return HttpMethodName.POST;
    }

    @Override
    public String uri() {
        return "/authentication/_login";
    }
    
    @Override
    public Map<String, String> headers() {
        Map<String, String> headers = super.headers();
        
        String basicAuth = javax.xml.bind.DatatypeConverter.printBase64Binary(
                (getUsername() + ":" + getPassword()).getBytes());
        
        headers.put(HttpHeaders.AUTHORIZATION, "Basic "+basicAuth);
        return headers;
    }
}
