package net.openesb.sdk.model;

import java.util.HashMap;
import java.util.Map;
import net.openesb.sdk.http.HttpMethodName;
import org.apache.http.HttpHeaders;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class GetComponentDescriptorRequest extends AbstractRequest {
    
    private final String componentName;
    
    public GetComponentDescriptorRequest(String componentName) {
        this.componentName = componentName;
    }

    public String getComponentName() {
        return componentName;
    }

    @Override
    public HttpMethodName method() {
        return HttpMethodName.GET;
    }

    @Override
    public Map<String, String> headers() {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put(HttpHeaders.ACCEPT, "application/xml");
        
        return headers;
    }

    @Override
    public String uri() {
        return "/components/" + getComponentName() + "/descriptor";
    }
}
