package net.openesb.sdk.model;

import java.util.Map;
import net.openesb.model.api.ComponentType;
import net.openesb.model.api.State;
import net.openesb.sdk.http.HttpMethodName;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ListComponentsRequest extends AbstractRequest {
    
    private ComponentType type;
    private State state;
    private String sharedLibraryName;
    private String serviceAssemblyName;

    public ComponentType getType() {
        return type;
    }

    public void setType(ComponentType type) {
        this.type = type;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getSharedLibraryName() {
        return sharedLibraryName;
    }

    public void setSharedLibraryName(String sharedLibraryName) {
        this.sharedLibraryName = sharedLibraryName;
    }

    public String getServiceAssemblyName() {
        return serviceAssemblyName;
    }

    public void setServiceAssemblyName(String serviceAssemblyName) {
        this.serviceAssemblyName = serviceAssemblyName;
    }

    @Override
    public HttpMethodName method() {
        return HttpMethodName.GET;
    }

    @Override
    public String uri() {
        return "/components";
    }

    @Override
    public Map<String, String> parameters() {
        Map<String, String> parameters = super.parameters();
        
        parameters.put("library", getSharedLibraryName());
        parameters.put("assembly", getServiceAssemblyName());

        if (getType() != null) {
            parameters.put("type", getType().name());
        }

        if (getState() != null) {
            parameters.put("state", getState().name());
        }
        
        return parameters;
    }
}
