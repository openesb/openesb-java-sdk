package net.openesb.sdk.model;

import java.util.Map;
import net.openesb.model.api.State;
import net.openesb.sdk.http.HttpMethodName;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ListServiceAssembliesRequest extends AbstractRequest {
    
    private State state;
    private String componentName;

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    @Override
    public HttpMethodName method() {
        return HttpMethodName.GET;
    }

    @Override
    public String uri() {
        return "/assemblies";
    }

    @Override
    public Map<String, String> parameters() {
        Map<String, String> parameters = super.parameters();
        if (getComponentName() != null) {
            parameters.put("component", getComponentName());
        }
        if (getState() != null) {
            parameters.put("state", getState().name());
        }
        return parameters;
    }
}
