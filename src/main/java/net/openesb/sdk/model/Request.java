package net.openesb.sdk.model;

import java.util.Map;
import net.openesb.sdk.http.HttpMethodName;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface Request {
    
    HttpMethodName method();
    
    Map<String, String> headers();
    
    String uri();
    
    Map<String, String> parameters();
}
