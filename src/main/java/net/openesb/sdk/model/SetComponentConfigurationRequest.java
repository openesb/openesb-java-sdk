package net.openesb.sdk.model;

import java.util.HashSet;
import java.util.Set;
import net.openesb.model.api.ComponentConfiguration;
import net.openesb.sdk.http.HttpMethodName;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class SetComponentConfigurationRequest extends AbstractRequest {
    
    private final String componentName;
    private Set<ComponentConfiguration> configurations = new HashSet<ComponentConfiguration>();
    
    public SetComponentConfigurationRequest(String componentName) {
        this.componentName = componentName;
    }

    public String getComponentName() {
        return componentName;
    }
    
    public void addComponentConfiguration(ComponentConfiguration configuration) {
        this.configurations.add(configuration);
    }
    
    public Set<ComponentConfiguration> getComponentConfigurations() {
        return new HashSet<ComponentConfiguration>(configurations);
    }

    @Override
    public HttpMethodName method() {
        return HttpMethodName.PUT;
    }

    @Override
    public String uri() {
        return "/components/" + getComponentName() + "/configuration";
    }
}
