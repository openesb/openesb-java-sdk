package net.openesb.sdk.model;

import net.openesb.sdk.http.HttpMethodName;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class GetServiceAssemblyRequest extends AbstractRequest {
    
    private final String serviceAssemblyName;
    
    public GetServiceAssemblyRequest(final String serviceAssemblyName) {
        this.serviceAssemblyName = serviceAssemblyName;
    }

    public String getServiceAssemblyName() {
        return serviceAssemblyName;
    }

    @Override
    public HttpMethodName method() {
        return HttpMethodName.GET;
    }

    @Override
    public String uri() {
        return "/assemblies/" + getServiceAssemblyName();
    }
}
