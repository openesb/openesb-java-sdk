package net.openesb.sdk.model;

import java.util.Map;
import net.openesb.sdk.http.HttpMethodName;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ListSharedLibrariesRequest extends AbstractRequest {
    
    private String componentName;

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    @Override
    public HttpMethodName method() {
        return HttpMethodName.GET;
    }

    @Override
    public String uri() {
        return "/libraries";
    }

    @Override
    public Map<String, String> parameters() {
        Map<String, String> parameters = super.parameters();
        if (getComponentName() != null) {
            parameters.put("component", getComponentName());
        }
        return parameters;
    }
}
