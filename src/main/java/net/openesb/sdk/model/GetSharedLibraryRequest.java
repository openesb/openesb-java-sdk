package net.openesb.sdk.model;

import net.openesb.sdk.http.HttpMethodName;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class GetSharedLibraryRequest extends AbstractRequest {
    
    private final String sharedLibraryName;
    
    public GetSharedLibraryRequest(final String sharedLibraryName) {
        this.sharedLibraryName = sharedLibraryName;
    }

    public String getSharedLibraryName() {
        return sharedLibraryName;
    }

    @Override
    public HttpMethodName method() {
        return HttpMethodName.GET;
    }

    @Override
    public String uri() {
        return "/libraries/" + getSharedLibraryName();
    }
}
