package net.openesb.sdk.model;

import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public abstract class AbstractRequest implements Request {

    private String username;
    private String password;
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    @Override
    public Map<String, String> headers() {
        HashMap<String, String> headers = new HashMap<String, String>();
        byte [] authentication = (username + ":" + password).getBytes();
        String header = DatatypeConverter.printBase64Binary(authentication);
        headers.put("Authorization", "Basic " + header);
        return headers;
    }

    @Override
    public Map<String, String> parameters() {
        return new HashMap<String, String>();
    }
}
