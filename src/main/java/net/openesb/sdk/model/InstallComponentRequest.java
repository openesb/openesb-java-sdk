package net.openesb.sdk.model;

import java.net.URL;
import net.openesb.sdk.http.HttpMethodName;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class InstallComponentRequest extends AbstractRequest {
    
    private final URL archiveUrl;
    
    public InstallComponentRequest(URL archiveUrl) {
        this.archiveUrl = archiveUrl;
    }

    public URL getArchiveUrl() {
        return archiveUrl;
    }

    @Override
    public HttpMethodName method() {
        return HttpMethodName.POST;
    }

    @Override
    public String uri() {
        return "/components";
    }
}
