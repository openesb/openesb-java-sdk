package net.openesb.sdk.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.openesb.sdk.http.HttpMethodName;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class DeleteComponentApplicationVariablesRequest extends AbstractRequest {
    
    private final String componentName;
    private Set<String> variables = new HashSet<String>();
    
    public DeleteComponentApplicationVariablesRequest(String componentName) {
        this.componentName = componentName;
    }

    public String getComponentName() {
        return componentName;
    }
    
    public void addApplicationVariable(String variable) {
        this.variables.add(variable);
    }
    
    public Set<String> getApplicationVariables() {
        return new HashSet<String>(variables);
    }
    
    @Override
    public HttpMethodName method() {
        return HttpMethodName.DELETE;
    }
    
    @Override
    public Map<String, String> parameters() {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("name", variables.iterator().next());
        return parameters;
    }

    @Override
    public String uri() {
        return "/components/" + getComponentName() + "/application/variables";
    }
}
