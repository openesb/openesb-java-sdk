package net.openesb.sdk.model;

import java.util.Map;
import net.openesb.sdk.http.HttpMethodName;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class UninstallComponentRequest extends AbstractRequest {
    
    private final String componentName;
    private boolean force;
    
    public UninstallComponentRequest(final String componentName) {
        this.componentName = componentName;
    }

    public boolean isForce() {
        return force;
    }

    public void setForce(boolean force) {
        this.force = force;
    }

    public String getComponentName() {
        return componentName;
    }

    @Override
    public HttpMethodName method() {
        return HttpMethodName.DELETE;
    }

    @Override
    public String uri() {
        return "/components/" + getComponentName();
    }

    @Override
    public Map<String, String> parameters() {
        Map<String, String> parameters = super.parameters();
        parameters.put("force", Boolean.toString(isForce()));
        return parameters;
    }
}
