package net.openesb.sdk.model;

import java.util.Map;
import net.openesb.sdk.http.HttpMethodName;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class UndeployServiceAssemblyRequest extends AbstractRequest {
    
    private final String serviceAssemblyName;
    private boolean force;
    
    public UndeployServiceAssemblyRequest(final String serviceAssemblyName) {
        this.serviceAssemblyName = serviceAssemblyName;
    }

    public boolean isForce() {
        return force;
    }

    public void setForce(boolean force) {
        this.force = force;
    }

    public String getServiceAssemblyName() {
        return serviceAssemblyName;
    }

    @Override
    public HttpMethodName method() {
        return HttpMethodName.DELETE;
    }

    @Override
    public String uri() {
        return "/assemblies/" + getServiceAssemblyName();
    }

    @Override
    public Map<String, String> headers() {
        Map<String, String> headers = super.headers();
        headers.put("Accept", "application/json");
        return headers;
    }
    
    @Override
    public Map<String, String> parameters() {
        Map<String, String> parameters = super.parameters();
        parameters.put("force", Boolean.toString(isForce()));
        return parameters;
    }
}
