package net.openesb.sdk.model;

import java.util.Map;
import net.openesb.sdk.http.HttpMethodName;
import org.apache.http.HttpHeaders;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class GetSharedLibraryDescriptorRequest extends AbstractRequest {
    
    private final String sharedLibraryName;
    
    public GetSharedLibraryDescriptorRequest(String sharedLibraryName) {
        this.sharedLibraryName = sharedLibraryName;
    }

    public String getSharedLibraryName() {
        return sharedLibraryName;
    }

    @Override
    public HttpMethodName method() {
        return HttpMethodName.GET;
    }

    @Override
    public String uri() {
        return "/libraries/" + getSharedLibraryName() + "/descriptor";
    }

    @Override
    public Map<String, String> headers() {
        Map<String, String> headers = super.headers();
        headers.put(HttpHeaders.ACCEPT, "application/xml");
        return headers;
    }
}
