package net.openesb.sdk.model;

import net.openesb.sdk.http.HttpMethodName;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class GetComponentRequest extends AbstractRequest {
    
    private final String componentName;
    
    public GetComponentRequest(final String componentName) {
        this.componentName = componentName;
    }

    public String getComponentName() {
        return componentName;
    }

    @Override
    public HttpMethodName method() {
        return HttpMethodName.GET;
    }

    @Override
    public String uri() {
        return "/components/" + getComponentName();
    }
}
