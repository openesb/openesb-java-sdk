package net.openesb.sdk.model;

import net.openesb.sdk.http.HttpMethodName;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class LogoutRequest extends AbstractRequest {

    @Override
    public HttpMethodName method() {
        return HttpMethodName.POST;
    }

    @Override
    public String uri() {
        return "/authentication/_logout";
    }
}
