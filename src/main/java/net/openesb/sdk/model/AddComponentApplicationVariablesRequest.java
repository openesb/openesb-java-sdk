package net.openesb.sdk.model;

import java.util.HashSet;
import java.util.Set;
import net.openesb.model.api.ApplicationVariable;
import net.openesb.sdk.http.HttpMethodName;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class AddComponentApplicationVariablesRequest extends AbstractRequest {
    
    private final String componentName;
    private Set<ApplicationVariable> variables = new HashSet<ApplicationVariable>();
    
    public AddComponentApplicationVariablesRequest(String componentName) {
        this.componentName = componentName;
    }

    public String getComponentName() {
        return componentName;
    }
    
    public void addApplicationVariable(ApplicationVariable variable) {
        this.variables.add(variable);
    }
    
    public Set<ApplicationVariable> getApplicationVariables() {
        return new HashSet<ApplicationVariable>(variables);
    }
    
    @Override
    public HttpMethodName method() {
        return HttpMethodName.PUT;
    }

    @Override
    public String uri() {
        return "/components/" + getComponentName() + "/application/variables";
    }
}
