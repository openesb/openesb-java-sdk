package net.openesb.sdk.model;

import java.util.Map;
import net.openesb.sdk.http.HttpMethodName;
import org.apache.http.HttpHeaders;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class GetServiceAssemblyDescriptorRequest extends AbstractRequest {
    
    private final String serviceAssemblyName;
    
    public GetServiceAssemblyDescriptorRequest(final String serviceAssemblyName) {
        this.serviceAssemblyName = serviceAssemblyName;
    }

    public String getServiceAssemblyName() {
        return serviceAssemblyName;
    }

    @Override
    public HttpMethodName method() {
        return HttpMethodName.GET;
    }

    @Override
    public String uri() {
        return "/assemblies/" + getServiceAssemblyName() + "/descriptor";
    }

    @Override
    public Map<String, String> headers() {
        Map<String, String> headers = super.headers();
        headers.put(HttpHeaders.ACCEPT, "application/xml");
        return headers;
    }
}
