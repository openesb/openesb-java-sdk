package net.openesb.sdk.model;

import java.net.URL;
import net.openesb.sdk.http.HttpMethodName;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class UpgradeComponentRequest extends AbstractRequest {
    
    private final URL archiveUrl;
    private String componentName;
    
    public UpgradeComponentRequest(final String componentName, final URL archiveUrl) {
        this.componentName = componentName;
        this.archiveUrl = archiveUrl;
    }

    public URL getArchiveUrl() {
        return archiveUrl;
    }

    public String getComponentName() {
        return componentName;
    }

    @Override
    public HttpMethodName method() {
        return HttpMethodName.PUT;
    }

    @Override
    public String uri() {
        return "/components/" + getComponentName() + "/configuration";
    }
}
