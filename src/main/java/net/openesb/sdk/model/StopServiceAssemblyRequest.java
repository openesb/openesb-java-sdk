package net.openesb.sdk.model;

import java.util.Map;
import net.openesb.sdk.http.HttpMethodName;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StopServiceAssemblyRequest extends AbstractRequest {
    
    private final String serviceAssemblyName;
    
    public StopServiceAssemblyRequest(final String serviceAssemblyName) {
        this.serviceAssemblyName = serviceAssemblyName;
    }

    public String getServiceAssemblyName() {
        return serviceAssemblyName;
    }

    @Override
    public HttpMethodName method() {
        return HttpMethodName.POST;
    }

    @Override
    public String uri() {
        return "/assemblies/" + getServiceAssemblyName();
    }

    @Override
    public Map<String, String> parameters() {
        Map<String, String> parameters = super.parameters();
        parameters.put("action", "stop");
        return parameters;
    }
}
