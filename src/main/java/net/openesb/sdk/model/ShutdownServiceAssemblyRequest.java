package net.openesb.sdk.model;

import java.util.Map;
import net.openesb.sdk.http.HttpMethodName;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ShutdownServiceAssemblyRequest extends AbstractRequest {
    
    private final String serviceAssemblyName;
    private boolean force;
    
    public ShutdownServiceAssemblyRequest(final String serviceAssemblyName) {
        this.serviceAssemblyName = serviceAssemblyName;
    }

    public String getServiceAssemblyName() {
        return serviceAssemblyName;
    }

    public boolean isForce() {
        return force;
    }

    public void setForce(boolean force) {
        this.force = force;
    }

    @Override
    public HttpMethodName method() {
        return HttpMethodName.POST;
    }

    @Override
    public String uri() {
        return "/assemblies/" + getServiceAssemblyName();
    }

    @Override
    public Map<String, String> parameters() {
        Map<String, String> parameters = super.parameters();
        parameters.put("action", "shutdown");
        parameters.put("force", Boolean.toString(isForce()));
        return parameters;
    }
}
