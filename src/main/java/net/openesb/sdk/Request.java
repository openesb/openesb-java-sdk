package net.openesb.sdk;

import net.openesb.sdk.http.HttpMethodName;

import java.io.InputStream;
import java.net.URI;
import java.util.Map;

/**
 * Represents a request being sent to the OpenESB Rest API, including the
 * parameters being sent as part of the request, the endpoint to which the
 * request should be sent, etc.
 *
 * @param <T> The type of original, user facing request represented by this
 * request.
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface Request<T> {

    /**
     * Adds the specified header to this request.
     *
     * @param name The name of the header to add.
     * @param value The header's value.
     */
    public void addHeader(String name, String value);

    /**
     * Returns a map of all the headers included in this request.
     *
     * @return A map of all the headers included in this request.
     */
    public Map<String, String> getHeaders();

    /**
     * Sets all headers, clearing any existing ones.
     */
    public void setHeaders(Map<String, String> headers);

    /**
     * Sets the path to the resource being requested.
     *
     * @param path The path to the resource being requested.
     */
    public void setResourcePath(String path);

    /**
     * Returns the path to the resource being requested.
     *
     * @return The path to the resource being requested.
     */
    public String getResourcePath();

    /**
     * Adds the specified request parameter to this request.
     *
     * @param name The name of the request parameter.
     * @param value The value of the request parameter.
     */
    public void addParameter(String name, String value);

    /**
     * Adds the specified request parameter to this request, and returns the
     * updated request object.
     *
     * @param name The name of the request parameter.
     * @param value The value of the request parameter.
     *
     * @return The updated request object.
     */
    public Request<T> withParameter(String name, String value);

    /**
     * Returns a map of all parameters in this request.
     *
     * @return A map of all parameters in this request.
     */
    public Map<String, String> getParameters();

    /**
     * Sets all parameters, clearing any existing values.
     */
    public void setParameters(Map<String, String> parameters);

    /**
     * Returns the service endpoint (ex: "http://localhost:4848") to which
     * this request should be sent.
     *
     * @return The service endpoint to which this request should be sent.
     */
    public URI getEndpoint();

    /**
     * Sets the service endpoint (ex: "http://localhost:4848") to which this
     * request should be sent.
     *
     * @param endpoint The service endpoint to which this request should be
     * sent.
     */
    public void setEndpoint(URI endpoint);

    /**
     * Returns the HTTP method (GET, POST, etc) to use when sending this
     * request.
     *
     * @return The HTTP method to use when sending this request.
     */
    public HttpMethodName getHttpMethod();

    /**
     * Sets the HTTP method (GET, POST, etc) to use when sending this request.
     *
     * @param httpMethod The HTTP method to use when sending this request.
     */
    public void setHttpMethod(HttpMethodName httpMethod);

    /**
     * Returns the optional stream containing the payload data to include for
     * this request. Not all requests will contain payload data.
     *
     * @return The optional stream containing the payload data to include for
     * this request.
     */
    public InputStream getContent();

    /**
     * Sets the optional stream containing the payload data to include for this
     * request. Not all requests will contain payload data.
     *
     * @param content The optional stream containing the payload data to include
     * for this request.
     */
    public void setContent(InputStream content);
    
    /**
     * Returns the original, user facing request object which this internal
     * request object is representing.
     *
     * @return The original, user facing request object which this request
     * object is representing.
     */
    //public AmazonWebServiceRequest getOriginalRequest();
}
