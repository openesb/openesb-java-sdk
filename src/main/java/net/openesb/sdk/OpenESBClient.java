package net.openesb.sdk;

import java.util.Set;
import net.openesb.model.api.*;
import net.openesb.sdk.model.*;

/**
 * Main entry point to the OpenESB SDK.
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface OpenESBClient {

    public String login(LoginRequest request) throws OpenESBClientException;
    public void logout(LogoutRequest request) throws OpenESBClientException;
    
    public Set<JBIComponent> listComponents(ListComponentsRequest request) throws OpenESBClientException;
    public JBIComponent getComponent(GetComponentRequest request) throws OpenESBClientException;
//    public ComponentDescriptor getComponentDescriptor(GetComponentDescriptorRequest request) throws OpenESBClientException;
    public String getComponentDescriptor(GetComponentDescriptorRequest request) throws Exception;
    public String installComponent(InstallComponentRequest request) throws Exception;
    public void upgradeComponent(UpgradeComponentRequest request) throws Exception;
    public void uninstallComponent(UninstallComponentRequest request) throws Exception;
    public void startComponent(StartComponentRequest request) throws Exception;
    public void stopComponent(StopComponentRequest request) throws Exception;
    public void shutdownComponent(ShutdownComponentRequest request) throws Exception;
    
    public Set<ApplicationVariable> listComponentApplicationVariables(ListComponentApplicationVariablesRequest request) throws Exception;
    public Set<ApplicationVariable> addComponentApplicationVariables(AddComponentApplicationVariablesRequest request) throws Exception;
    public Set<ApplicationVariable> updateComponentApplicationVariables(UpdateComponentApplicationVariablesRequest request) throws Exception;
    public Set<ApplicationVariable> deleteComponentApplicationVariables(DeleteComponentApplicationVariablesRequest request) throws Exception;
    
    public void setComponentConfigurations(SetComponentConfigurationRequest request) throws Exception;
    public Set<ComponentConfiguration> listComponentConfigurations(ListComponentConfigurationsRequest request) throws Exception;
    
    public void setComponentLogger(SetComponentLoggerRequest request) throws Exception;
    public Set<Logger> listComponentLoggers(ListComponentLoggersRequest request) throws Exception;
    
    public Set<ServiceAssembly> listServiceAssemblies(ListServiceAssembliesRequest request) throws Exception;
    public String deployServiceAssembly(DeployServiceAssemblyRequest request) throws Exception;
    public void undeployServiceAssembly(UndeployServiceAssemblyRequest request) throws Exception;
    public ServiceAssembly getServiceAssembly(GetServiceAssemblyRequest request) throws Exception;
    public String getServiceAssemblyDescriptor(GetServiceAssemblyDescriptorRequest request) throws Exception;
    public void startServiceAssembly(StartServiceAssemblyRequest request) throws Exception;
    public void stopServiceAssembly(StopServiceAssemblyRequest request) throws Exception;
    public void shutdownServiceAssembly(ShutdownServiceAssemblyRequest request) throws Exception;
    
    public Set<SharedLibrary> listSharedLibraries(ListSharedLibrariesRequest request) throws Exception;
    public SharedLibrary getSharedLibrary(GetSharedLibraryRequest request) throws Exception;
    public String getSharedLibraryDescriptor(GetSharedLibraryDescriptorRequest request) throws Exception;
//    public ComponentDescriptor getSharedLibraryDescriptor(GetSharedLibraryDescriptorRequest request) throws Exception;
    public String installSharedLibrary(InstallSharedLibraryRequest request) throws Exception;
    public void uninstallSharedLibrary(UninstallSharedLibraryRequest request) throws Exception;
}
