package net.openesb.sdk.http;

import net.openesb.model.api.JBIComponent;
import net.openesb.sdk.OpenESBClient;
import net.openesb.sdk.model.GetComponentDescriptorRequest;
import net.openesb.sdk.model.GetComponentRequest;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class OpenESBHttpClientTest {
 
    private final OpenESBClient client = new OpenESBClientImpl();
    
    /*
    @Test
    public void listComponents() throws Exception {
        ListComponentsRequest req = new ListComponentsRequest();
        Set<JBIComponent> components = client.listComponents(req);
        
        Assert.assertNotNull(components);
    }
    */

//    @Test
    public void getComponent() throws Exception {
        JBIComponent comp = client.getComponent(new GetComponentRequest("sun-bpel-engine"));
        
        Assert.assertNotNull(comp);
        System.out.println(comp);
        
        String descriptor = client.getComponentDescriptor(new GetComponentDescriptorRequest("sun-bpel-engine"));
        System.out.println(descriptor);
        
        /*
        Set<ApplicationVariable> variables = client.listApplicationVariables(new ListApplicationVariablesRequest(comp.getName()));
        System.out.println(variables);
        
        AddApplicationVariablesRequest addVarReq= new AddApplicationVariablesRequest(comp.getName());
        
        addVarReq.addApplicationVariable(new ApplicationVariable("toto", "test", "string"));
        addVarReq.addApplicationVariable(new ApplicationVariable("toto2", "test2", "string"));
        
        client.addApplicationVariables(addVarReq);
        */ 
    }
}
